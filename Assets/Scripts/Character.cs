﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Character : MonoBehaviour
{
    public float Speed = 0.0f;
    public float lateralMovement = 3.0f;
    public float jumpMovement = 400.0f;
    private float movementButton = 0.0f;
    public Transform groundCheck;

    private Animator animator;
    private Rigidbody2D rigidbody2d;

    public bool grounded = true;
    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //grounded = Physics2D.Linecast(transform.position, groundCheck.position, LayerMask.GetMask("Ground"));
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        if (grounded && Input.GetButtonDown("Jump"))
        {
            rigidbody2d.AddForce(Vector2.up * jumpMovement);
        }

        if (grounded)
        {
            animator.SetTrigger("Grounded");
        }
        else
        {
            animator.SetTrigger("Jump");
        }

        //Speed = lateralMovement * Input.GetAxis("Horizontal");
        Speed = lateralMovement * movementButton;
        transform.Translate(Vector2.right * Speed * Time.deltaTime);
        animator.SetFloat("Speed", Mathf.Abs(Speed));

        if (Speed < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }

    }

    void OnTriggerEnter2D(Collider2D collider)
    {

        if (collider.gameObject.tag == "MobilePlatform")
        {
            transform.SetParent(collider.transform);
        }
        if (collider.tag == "Zoom")
        {
            GameObject.Find("MainVirtual").GetComponent<CinemachineVirtualCamera>().enabled = false;
        }
        
        if (collider.gameObject.tag == "Star")
        {
            GameStatus.numStars++;
            Destroy(collider.gameObject);
        }
        if (GameStatus.numStars == 2)
        {
            print("Has conseguido 2 estrellas");
        }
        
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == "Zoom")
        {
            GameObject.Find("MainVirtual").GetComponent<CinemachineVirtualCamera>().enabled = true;
        }
        
        if (collider.gameObject.tag == "MobilePlatform")
        {
            transform.SetParent(null);
        }
    }

    public void jump()
    {
        if (grounded)
        {
            rigidbody2d.AddForce(Vector2.up * jumpMovement);
        }
    }

    public void Move(float amount)
    {
        movementButton = amount;
    }
    
}
